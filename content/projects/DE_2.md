+++
title = "CRUD Operations in SQLite DB using Python and CLI"
date = 2023-09-01
[taxonomies]
categories=["Data Engineering"]
tags=["Python", "SQL"]
+++

Link to Github Repository: [Link](https://github.com/nogibjj/IDS-706_rg361_week-5)

## Project Overview:
This project repositroy contains files to perform CRUD (Create-Write-Update-Delete) operations in a SQLite Database using Python and CLI

The repository has the main.py file which makes use of the files in the mylib folder to perform CRUD operations on a SQLite Database. The main.py file can be interacted via CLI (Command Line Interface) by the user.

The repository automatically logs all the queries which are executed in the query_logs file.

Github actions automatically runs the test_main.py which triggers the CRUD operations and logs them whenever there is an update in the repository.
