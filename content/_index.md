+++
title = "Website title"
description = "Personnal blog"
date = 2021-11-12
+++

I am a Data Science student at Duke University with a passion for leveraging data-driven insights to solve complex problems. With 3.5 years of experience in consulting and data analytics, I have honed my skills in interpreting and analyzing data to provide strategic recommendations.
